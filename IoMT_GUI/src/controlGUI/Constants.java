package controlGUI;

public class Constants {
	public static final String UP_BUTTON = "u";
	public static final String DOWN_BUTTON = "d";
	public static final String LEFT_BUTTON = "l";
	public static final String RIGHT_BUTTON = "r";
	public static final int UP = 0;
	public static final int DOWN = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
}