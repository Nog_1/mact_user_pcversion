package controlGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.HashMap;

import controlGUI.Constants;

public class PadListener implements ActionListener{
	
	ControlPanel cp;
	
	public PadListener(ControlPanel cp){
		this.cp = cp;
	}

	@SuppressWarnings("serial")
	private HashMap<String, Integer> controlMap = new HashMap<String, Integer>(){{
		put(Constants.UP_BUTTON, Constants.UP);
		put(Constants.DOWN_BUTTON, Constants.DOWN);
		put(Constants.LEFT_BUTTON, Constants.LEFT);
		put(Constants.RIGHT_BUTTON, Constants.RIGHT);
	}};

	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch(controlMap.get(arg0.getActionCommand())){
			case Constants.UP:
				cp.ctxnetClient.sendCommand("UP");
				break;
			case Constants.DOWN:
				cp.ctxnetClient.sendCommand("DWN");
				break;
			case Constants.LEFT:
				cp.ctxnetClient.sendCommand("LEFT");
				break;
			case Constants.RIGHT:
				cp.ctxnetClient.sendCommand("RIGHT");
				break;
			default:
				cp.ctxnetClient.sendCommand("Some nasty stuff hppnd");
				break;
		}
		
		cp.window.requestFocus();
	}
}