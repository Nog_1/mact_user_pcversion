package controlGUI;

import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.PlainDocument;

import actuatorHandler.Actuator;
import contextNET.ControllerClient;

import javax.swing.JLabel;

public class ControlPanel{
	
	static ControlPanel CP;
	ControllerClient ctxnetClient;
	Window window;
	JTextField Rtext;
	JTextField Gtext;
	JTextField Btext;
	private JComboBox<String> smarThingsList = new JComboBox<String>();
	   
	private JButton addAButton(String title, Container container, int width, int height, float alignment) {
		JButton button = new JButton(title);
		Dimension dimension = new Dimension(width, height);
		
		button.setMinimumSize(dimension);
		button.setPreferredSize(dimension);
		button.setMaximumSize(dimension);
		button.setAlignmentX(alignment);
		container.add(button);
		
		return button;
	}
	
	private JTextField addIntTextField(String text, Container container, float alignment, String label, Color color) {
		JTextField textField = new JTextField("", 25);
		JLabel jLabel = new JLabel(label);
		PlainDocument doc;
		
		jLabel.setForeground(color);
		textField.setMaximumSize(new Dimension(50, (int)textField.getPreferredSize().getHeight()));
		textField.setAlignmentX(Component.CENTER_ALIGNMENT);
		container.add(jLabel);
		container.add(textField);
		
		doc = (PlainDocument) textField.getDocument();
	    doc.setDocumentFilter(new MyIntFilter());
		
		return textField;
	}

	private ControlPanel(ControllerClient client) {
		this.ctxnetClient = client;
		window = new Window(this);
		Container mainPanel = window.getContentPane();
		JPanel smarThings = new JPanel();
		JPanel buttonPad = new JPanel();
		JPanel leftAndRightButtons = new JPanel();
		JPanel RGBFields = new JPanel();
		JButton button;
		PadListener padListener = new PadListener(this);
		Actuator robotCar = new Actuator("cmd", "EVA_01", "robot car", "");

		Dimension minSize = new Dimension(20, 20);
		Dimension prefSize = new Dimension(20, 20);
		Dimension maxSize = new Dimension(20, 20);
		
		//WINDOW JFRAME OPTIONS
		window.setTitle("Control Unit");
		window.setLocationRelativeTo(null);
		window.setResizable(false);
		window.setVisible(true);
		window.setSize(350, 350);
		//window.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.BLUE));
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//MAIN CONTAINER SET TO BOXLAYOUT (VERTICAL)
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBackground(Color.lightGray);
		mainPanel.add(new Box.Filler(minSize, prefSize, maxSize));
		
		//SMART STUFF COMBO BOX
		smarThings.setLayout(new BoxLayout(smarThings, BoxLayout.LINE_AXIS));
		smarThings.setBackground(Color.lightGray);
		smarThings.add(new JLabel("Choose smart thing:"));
		minSize = new Dimension(10, 10); prefSize = new Dimension(10, 10); maxSize = new Dimension(10, 10);  //NEW DIMENSIONS TO FILL
		smarThings.add(new Box.Filler(minSize, prefSize, maxSize));//SPACE FILLING
		minSize = new Dimension(100, 30);
		smarThingsList.setMinimumSize(minSize);
		smarThingsList.setMaximumSize(minSize);
		smarThingsList.addItem(robotCar.toString());
		smarThings.add(smarThingsList);
		mainPanel.add(smarThings);
		
		minSize = new Dimension(10, 10); prefSize = new Dimension(10, 10); maxSize = new Dimension(10, 10);  //NEW DIMENSIONS TO FILL
		mainPanel.add(new Box.Filler(minSize, prefSize, maxSize));//SPACE FILLING
		
		//BUTTON PAD
		buttonPad.setLayout(new BoxLayout(buttonPad, BoxLayout.PAGE_AXIS));
		//UP_BUTTON
		minSize = new Dimension(10, 10); prefSize = new Dimension(10, 10); maxSize = new Dimension(10, 10);  //NEW DIMENSIONS TO FILL
		buttonPad.add(new Box.Filler(minSize, prefSize, maxSize));//SPACE FILLING
		button = addAButton("▲", buttonPad, 50, 50, Component.CENTER_ALIGNMENT);
		button.setActionCommand(Constants.UP_BUTTON);
		button.addActionListener(padListener);
		//LEFT & RIGHT BUTTONS
		leftAndRightButtons.setLayout(new BoxLayout(leftAndRightButtons, BoxLayout.LINE_AXIS));
		minSize = new Dimension(10, 10); prefSize = new Dimension(10, 10); maxSize = new Dimension(10, 10);  //NEW DIMENSIONS TO FILL
		leftAndRightButtons.add(new Box.Filler(minSize, prefSize, maxSize));//SPACE FILLING
		button = addAButton("◀", leftAndRightButtons, 50, 50, Component.CENTER_ALIGNMENT);
		button.setActionCommand(Constants.LEFT_BUTTON);
		button.addActionListener(padListener);
		minSize = new Dimension(50,50); prefSize = new Dimension(50,50); maxSize = new Dimension(50,50); //NEW DIMENSIONS TO FILL
		leftAndRightButtons.add(new Box.Filler(minSize, prefSize, maxSize)); //SPACE FILLING
		button = addAButton("▶", leftAndRightButtons, 50, 50, Component.CENTER_ALIGNMENT);
		button.setActionCommand(Constants.RIGHT_BUTTON);
		button.addActionListener(padListener);
		minSize = new Dimension(10, 10); prefSize = new Dimension(5, 5); maxSize = new Dimension(10, 10);  //NEW DIMENSIONS TO FILL
		leftAndRightButtons.add(new Box.Filler(minSize, prefSize, maxSize));//SPACE FILLING
		buttonPad.add(leftAndRightButtons);
		//DOWN BUTTON
		button = addAButton("▼", buttonPad, 50, 50, Component.CENTER_ALIGNMENT);
		button.setActionCommand(Constants.DOWN_BUTTON);
		button.addActionListener(padListener);
		minSize = new Dimension(10, 10); prefSize = new Dimension(10, 10); maxSize = new Dimension(10, 10);  //NEW DIMENSIONS TO FILL
		buttonPad.add(new Box.Filler(minSize, prefSize, maxSize));//SPACE FILLING
		buttonPad.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.darkGray));
		mainPanel.add(buttonPad);
		
		minSize = new Dimension(10, 10); prefSize = new Dimension(10, 10); maxSize = new Dimension(10, 10);  //NEW DIMENSIONS TO FILL
		mainPanel.add(new Box.Filler(minSize, prefSize, maxSize));//SPACE FILLING
	
		//RGB FIELD TEXTS
		RGBFields.setLayout(new BoxLayout(RGBFields, BoxLayout.LINE_AXIS));
		RGBFields.setBackground(Color.lightGray);
		Rtext = addIntTextField("Red", RGBFields, Component.CENTER_ALIGNMENT, "Red:", Color.RED);
		minSize = new Dimension(20, 20); prefSize = new Dimension(20, 20); maxSize = new Dimension(20, 20); //NEW DIMENSIONS TO FILL
		RGBFields.add(new Box.Filler(minSize, prefSize, maxSize)); //SPACE FILLING
		Gtext = addIntTextField("Green", RGBFields, Component.CENTER_ALIGNMENT, "Green:", new Color(0,200,0));
		minSize = new Dimension(20, 20); prefSize = new Dimension(20, 20); maxSize = new Dimension(20, 20); //NEW DIMENSIONS TO FILL
		RGBFields.add(new Box.Filler(minSize, prefSize, maxSize)); //SPACE FILLING
		Btext = addIntTextField("Blue", RGBFields, Component.CENTER_ALIGNMENT, "Blue:", Color.BLUE);
		mainPanel.add(RGBFields);
		
		//RGB SEND BUTTON
		minSize = new Dimension(20, 20); prefSize = new Dimension(20, 20); maxSize = new Dimension(20, 20); //NEW DIMENSIONS TO FILL
		mainPanel.add(new Box.Filler(minSize, prefSize, maxSize)); //SPACE FILLING
		
		button = addAButton("Send RGB", mainPanel, 120, 20, Component.CENTER_ALIGNMENT);
		button.addActionListener(new SendRgbListener(CP));
		
		//RGB send
		//RGBSend.setLayout(new BoxLayout(RGBFields, BoxLayout.LINE_AXIS));
		window.revalidate();
		window.repaint();
		window.requestFocus();
	}

	/**
	 * Creates a Window component and adds its elements to construct 
	 * the GUI controller which controls the selected smart thing;
	 * Singleton;
	 * Receives client object from ContextNET as parameter;
	 * Returns Control Panel object.
	 * @param ControllerClient
	 * @return ControlPanel
	 */
	public static ControlPanel getControlPanel(ControllerClient client) {
		if(CP == null)
			CP = new ControlPanel(client);
		return CP;
	}
}