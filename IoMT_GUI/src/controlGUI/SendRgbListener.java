package controlGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controlGUI.ControlPanel;

public class SendRgbListener implements ActionListener{
	
	ControlPanel cp;
	
	public SendRgbListener(ControlPanel cp) {
		this.cp = cp;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		cp.ctxnetClient.sendCommand(cp.Rtext.getText());
		cp.ctxnetClient.sendCommand(cp.Gtext.getText());
		cp.ctxnetClient.sendCommand(cp.Btext.getText());
		cp.window.requestFocus();
	}
}