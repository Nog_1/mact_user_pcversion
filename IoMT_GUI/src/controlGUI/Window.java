package	controlGUI;

import	java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Set;

import	javax.swing.JFrame;

@SuppressWarnings("serial")
public class Window	extends	JFrame{
	
	Window window;
	Set<Integer> pressed = new HashSet<>();
	
	public Window(ControlPanel cp){
		window = this;
		this.requestFocus();

		addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e)
			{
				window.requestFocus();
				System.out.println("mouse event triggered");
			}
		});
		
		addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				pressed.add(e.getKeyCode());
				if(pressed.size() >= 1) {
					for(Integer c : pressed) {
						switch(c) {
							case KeyEvent.VK_LEFT:
								if(pressed.contains(KeyEvent.VK_RIGHT))
									break;
								cp.ctxnetClient.sendCommand("LEFT");
								break;
								
							case KeyEvent.VK_RIGHT:
								if(pressed.contains(KeyEvent.VK_LEFT))
									break;
								cp.ctxnetClient.sendCommand("RIGHT");
								break;
								
							case KeyEvent.VK_UP:
								if(pressed.contains(KeyEvent.VK_DOWN))
									break;
								cp.ctxnetClient.sendCommand("UP");
								break;
								
							case KeyEvent.VK_DOWN:
								if(pressed.contains(KeyEvent.VK_UP))
									break;
								cp.ctxnetClient.sendCommand("DOWN");
								break;
								
							case KeyEvent.VK_ENTER:
								cp.ctxnetClient.sendCommand("ENTER");
								break;
								
							default:
								break;
						}
					}
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				pressed.remove(e.getKeyCode());
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
