package main;

import contextNET.ControllerClient;
import controlGUI.ControlPanel;

public class Main {
	@SuppressWarnings("unused")
	public static void main(String[] args){
		ControllerClient client = new ControllerClient();
		ControlPanel cp = ControlPanel.getControlPanel(client);
	}
}